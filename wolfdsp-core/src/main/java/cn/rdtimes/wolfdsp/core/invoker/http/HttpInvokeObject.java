package cn.rdtimes.wolfdsp.core.invoker.http;

import cn.rdtimes.wolfdsp.core.data.BalanceStrategy;
import cn.rdtimes.wolfdsp.core.data.TaskInstance;

import java.util.Map;

/**
 * http调用对象
 *
 * @author BZ
 */
public class HttpInvokeObject {
    private String jobId;
    private String jobInstanceId;
    private String taskId;
    private String taskInstanceId;
    // ip为空值将按微服务名称调用
    private String ip;
    private int port;
    private boolean hasTls;
    // 输入参数,整合后的
    private Map<String, Object> inputParam;
    // 微服务名称
    private String serviceName;
    private BalanceStrategy balanceStrategy;

    public HttpInvokeObject() {
    }

    public void set(TaskInstance instance) {
        this.jobId = instance.getJobId();
        this.taskId = instance.getTaskId();
        this.ip = instance.getIp();
        this.port = instance.getPort();
        this.serviceName = instance.getServiceName();
        this.balanceStrategy = instance.getBalanceStrategy();
        this.jobInstanceId = instance.getJobInstanceId();
        this.taskInstanceId = instance.getInstanceId();
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public boolean isHasTls() {
        return hasTls;
    }

    public void setHasTls(boolean hasTls) {
        this.hasTls = hasTls;
    }

    public Map<String, Object> getInputParam() {
        return inputParam;
    }

    public void setInputParam(Map<String, Object> inputParam) {
        this.inputParam = inputParam;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public BalanceStrategy getBalanceStrategy() {
        return balanceStrategy;
    }

    public void setBalanceStrategy(BalanceStrategy balanceStrategy) {
        this.balanceStrategy = balanceStrategy;
    }

    public String getJobInstanceId() {
        return jobInstanceId;
    }

    public void setJobInstanceId(String jobInstanceId) {
        this.jobInstanceId = jobInstanceId;
    }

    public String getTaskInstanceId() {
        return taskInstanceId;
    }

    public void setTaskInstanceId(String taskInstanceId) {
        this.taskInstanceId = taskInstanceId;
    }

    @Override
    public String toString() {
        return "HttpInvokeObject{" +
                "jobId='" + jobId + '\'' +
                ", jobInstanceId='" + jobInstanceId + '\'' +
                ", taskId='" + taskId + '\'' +
                ", taskInstanceId='" + taskInstanceId + '\'' +
                ", ip='" + ip + '\'' +
                ", port=" + port +
                ", hasTls=" + hasTls +
                ", inputParam=" + inputParam +
                ", serviceName='" + serviceName + '\'' +
                ", balanceStrategy=" + balanceStrategy +
                '}';
    }

}
