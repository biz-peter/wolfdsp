package cn.rdtimes.wolfdsp.core.invoker;

import cn.rdtimes.wolfdsp.core.util.SimpleList;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * 任务执行处理器工程
 *
 * @author BZ
 */
public class TaskInvokeHandlerFactory {

    /**
     * key = 调用执行类型,value=类型下对应的处理器实现列表
     */
    private static final Map<TaskInvokeHandler.InvokeKind, SimpleList<TaskInvokeHandler<?,?>>>
            handlerMap = new HashMap<>(3);

    public static void registry(TaskInvokeHandler<?,?> handler) {
        TaskInvokeHandler.InvokeKind kind = handler.getKind();
        SimpleList<TaskInvokeHandler<?,?>> sl;
        if (handlerMap.containsKey(kind)) {
            sl = handlerMap.get(kind);
            if (!sl.contains(handler.getName())) {
                sl.add(handler.getName(), handler);
            } else {
                sl.set(handler.getName(), handler);
            }
        } else {
            sl = new SimpleList<>();
            sl.add(handler.getName(), handler);
            handlerMap.put(kind, sl);
        }
    }

    public static void unregistry(TaskInvokeHandler<?,?> handler) {
        TaskInvokeHandler.InvokeKind kind = handler.getKind();
        SimpleList<TaskInvokeHandler<?,?>> sl = handlerMap.get(kind);
        if (sl != null) {
            sl.remove(handler.getName());
        }
    }

    public static TaskInvokeHandler<?, ?> get(TaskInvokeHandler.InvokeKind kind, String id) {
        SimpleList<TaskInvokeHandler<?,?>> sl = handlerMap.get(kind);
        if (sl != null) {
            return sl.get(id);
        }
        return null;
    }

    public static TaskInvokeHandler<?, ?> getFirst(TaskInvokeHandler.InvokeKind kind) {
        SimpleList<TaskInvokeHandler<?,?>> sl = handlerMap.get(kind);
        if (sl != null) {
            return sl.getFirst();
        }
        return null;
    }

    public static Set<Map.Entry<TaskInvokeHandler.InvokeKind, SimpleList<TaskInvokeHandler<?,?>>>> entrySet() {
        return handlerMap.entrySet();
    }

    public static void createDefaultTaskInvokeHandler() {
        new DefaultHttpTaskInvokeHandler();
        new DefaultJarTaskInvokeHandler();
        new DefaultShellTaskInvokeHandler();
    }

}
