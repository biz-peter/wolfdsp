package cn.rdtimes.wolfdsp.core.data;

import cn.rdtimes.wolfdsp.core.service.Service;

/**
 * 数据服务提供者接口
 *
 * @author BZ
 */
public interface DataService extends Service {

    default String getName() {
        return "DataService";
    }

    /**
     * @return 返回调度流程定义数据服务
     */
    DataDefinitionService getDefinitionService();

    /**
     * @return 返回调度流程实例服务
     */
    DataInstanceService getInstanceService();
}
