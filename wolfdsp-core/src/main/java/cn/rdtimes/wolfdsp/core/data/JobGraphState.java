package cn.rdtimes.wolfdsp.core.data;

/**
 * 任务流程执行时的状态
 *
 * @author BZ
 */
public enum JobGraphState {
    UNSCHEDULED(0, "未调度"),
    RUNNING(1, "运行中"),
    FINISHED(2, "正常完成"),
    FAILED(3, "失败或异常"),
    KILLED(4, "已杀死或已停止"),
    ;

    private final int value;
    private final String name;

    JobGraphState(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public int getValue() {
        return value;
    }


    public String getName() {
        return name;
    }

    public static JobGraphState of(int value) {
        for (JobGraphState ts : values()) {
            if (ts.getValue() == value) {
                return ts;
            }
        }
        return JobGraphState.UNSCHEDULED;
    }

}
