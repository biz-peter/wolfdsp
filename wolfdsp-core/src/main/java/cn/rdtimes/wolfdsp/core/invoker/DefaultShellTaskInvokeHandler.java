package cn.rdtimes.wolfdsp.core.invoker;

/**
 * 缺省实现执行本地shell脚本
 *
 * @author BZ
 */
class DefaultShellTaskInvokeHandler extends AbstractTaskInvokeHandler<String, AbstractTaskInvokeHandler.LocalResponse> {

    public DefaultShellTaskInvokeHandler() {
        super(InvokeKind.SHELL, "DefaultShellTaskInvokeHandler");
    }

    @Override
    public LocalResponse invoke(String invokeObject) throws Exception {
        String[] cmds = parseCommandLine(invokeObject);
        ShellCommand command = ShellCommand.builder().setCommands(cmds).build();
        StringBuilder sb = command.execute();
        return new LocalResponse(command.exitCode(), sb.toString());
    }

}
