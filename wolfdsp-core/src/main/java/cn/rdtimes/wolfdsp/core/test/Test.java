package cn.rdtimes.wolfdsp.core.test;

import cn.rdtimes.wolfdsp.core.counter.PrefCounter;
import cn.rdtimes.wolfdsp.core.ha.PlatformHAStrategy;
import cn.rdtimes.wolfdsp.core.ids.DefaultIdsGenerator;
import cn.rdtimes.wolfdsp.core.job.CronExpression;
import cn.rdtimes.wolfdsp.core.job.JobGraphService;
import cn.rdtimes.wolfdsp.core.service.ScheduleManager;

import java.util.Date;

/**
 * 测试类
 *
 * @author BZ
 */
public class Test {

    public static void test1() throws Exception {
        // 全局管理器,设置相关服务
        ScheduleManager manager = ScheduleManager.getInstance();
        // 0.设置配置，必须设置
        //manager.setConfiguration(new Configuration());
        //manager.setNameService(new NameService());

        //1.下面服务可选，不设置使用默认实现
        //manager.setPlatformHAStrategy(null);
        //manager.setDataService(null);
        //manager.setIdsGenerator(null);
        //manager.setNameService(null);
        //manager.setStateSyncService(null)

        // 3.添加自定义调用执行器,并且修改配置中指定自定id选项
        manager.setTaskInvokeHandler(null /* new MyHttpTaskInvokeHandler() */);
        manager.setTaskInvokeHandler(null /* new MyJarTaskInvokeHandler() */);
        manager.setTaskInvokeHandler(null /* new MyShellTaskInvokeHandler() */);

        //4.启动管理器
        manager.start();


        ////////////////可以开始调度了////////////////
        JobGraphService graphService = manager.getJobGraphService();
        graphService.schedule("job-app-id-1");// 创建调度实例

        // add ...

        PrefCounter[] counters = graphService.getPrefCounter();
    }

    static void test2() throws Exception {
        DefaultIdsGenerator ids = new DefaultIdsGenerator();
        for (int i = 0; i < 5; i++) {
            System.out.println(ids.generateJobDefinitionId());
            System.out.println(ids.generateJobInstanceId());
            System.out.println(ids.generateTaskDefinitionId());
            System.out.println(ids.generateTaskInstanceId());
            System.out.println("======================");

            Thread.sleep(100);
        }
    }

    static void test3() throws Exception {
        String cron = "5 13 9 29 3 ? 2022";
        CronExpression cronExpression = new CronExpression(cron);

        Date now = new Date();
        Date next = cronExpression.getNextInvalidTimeAfter(now);
        long delta = next.getTime() - now.getTime();
        System.out.println(next);

        long next1 = next.getTime() + delta;
        next = cronExpression.getTimeAfter(new Date(next1));
        System.out.println(next);
    }


    public static void main(String[] args) {
        //test1();
    }

}
