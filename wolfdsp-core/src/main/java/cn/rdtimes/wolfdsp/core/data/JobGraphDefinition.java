package cn.rdtimes.wolfdsp.core.data;

import java.io.Serializable;

/**
 * 任务流程定义
 *
 * @author BZ
 */
public class JobGraphDefinition implements Serializable {
    private static final long serialVersionUID = -1;

    // 唯一标识
    private String jobId;
    // 名称
    private String jobName;
    // 是否可以在失败或异常处继续执行
    private boolean enabledContinue;
    // 流程参数json字符串，格式：{"xxx":"yyy","xxx1":"yyy1"...}
    private String paramJson;
    // cron调度表达式
    private String cronExp;

    public JobGraphDefinition() {
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public boolean isEnabledContinue() {
        return enabledContinue;
    }

    public void setEnabledContinue(boolean enabledContinue) {
        this.enabledContinue = enabledContinue;
    }

    public String getParamJson() {
        return paramJson;
    }

    public void setParamJson(String paramJson) {
        this.paramJson = paramJson;
    }

    public String getCronExp() {
        return cronExp;
    }

    public void setCronExp(String cronExp) {
        this.cronExp = cronExp;
    }

    @Override
    public String toString() {
        return "JobGraphDefinition{" +
                "jobId='" + jobId + '\'' +
                ", jobName='" + jobName + '\'' +
                ", enabledContinue=" + enabledContinue +
                ", paramJson='" + paramJson + '\'' +
                ", cronExp='" + cronExp + '\'' +
                '}';
    }
}
