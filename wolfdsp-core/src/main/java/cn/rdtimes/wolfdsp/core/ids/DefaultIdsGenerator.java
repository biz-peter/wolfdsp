package cn.rdtimes.wolfdsp.core.ids;

/**
 * 缺省实现,使用系统当前时间戳
 *
 * @author BZ
 */
public class DefaultIdsGenerator implements IdsGenerator {

    public DefaultIdsGenerator() {
    }

    @Override
    public String generateJobDefinitionId() {
        return JOB_DEFINITION_ID_PREF + System.currentTimeMillis();
    }

    @Override
    public String generateJobInstanceId() {
        return JOB_INSTANCE_ID_PREF + System.currentTimeMillis();
    }

    @Override
    public String generateTaskDefinitionId() {
        return TASK_DEFINITION_ID_PRE + System.currentTimeMillis();
    }

    @Override
    public String generateTaskInstanceId() {
        return TASK_INSTANCE_ID_PREF + System.currentTimeMillis();
    }

}
