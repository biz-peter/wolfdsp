package cn.rdtimes.wolfdsp.core.data;

import cn.rdtimes.wolfdsp.core.service.Service;

import java.util.List;

/**
 * 数据任务实例服务
 * 这里数据对象与数据存储直接相关，实际存储数据按需扩展
 *
 * @author BZ
 */
public interface DataInstanceService extends Service {

    default String getName() {
        return "DataInstanceService";
    }

    /**
     * 批量装载已经存在的任务流程实例类
     */
    class JobGraphInstanceIds {
        public String instanceId;
        public String jobId;
        public JobGraphState jobGraphState;
    }

    /**
     * 从数据存储中装载指定的任务流程实例，且状态为未调度或正在运行中
     *
     * @param jobId 任务流程id
     * @return 返回任务流程实例，可以为空值
     * @throws Exception 失败抛出异常
     */
    JobGraphInstanceIds loadJobGraphInstanceIdAndRunning(String jobId) throws Exception;

    /**
     * 从数据存储中装载指定的调度流程id， 以便能够为正常结束的继续执行， 否则抛出异常
     *
     * @param instanceId 流程实例id
     * @param jobId      流程id
     * @return 流程实例对象
     * @throws Exception 失败抛出异常
     */
    JobGraphInstance loadJobGraphInstance(String instanceId, String jobId) throws Exception;

    /**
     * 插入调度流程实例对象到数据村粗
     *
     * @param instance 流程对象
     * @return 影响条数
     * @throws Exception 失败抛出异常
     */
    int insertJobGraphInstance(JobGraphInstance instance) throws Exception;

    /**
     * 修改指定的流程实例对象到数据存储
     *
     * @param instanceId     流程实例id
     * @param jobId          流程id
     * @param jobGraphState  状态
     * @param completedCount 完成任务数量
     * @param failCount      失败任务数量
     * @param skipCount      跳过任务数量
     * @param startTime      开始时间戳
     * @param endTime        结束时间戳
     * @param progress       进度
     * @param exceptionInfo  异常信息，有异常时应该不为空值
     * @return 影响条数
     * @throws Exception 失败抛出异常
     */
    int updateJobGraphInstance(String instanceId, String jobId, JobGraphState jobGraphState,
                               int completedCount, int failCount, int skipCount, long startTime,
                               Long endTime, int progress, String exceptionInfo) throws Exception;

    /**
     * 修改指定的流程实例对象的进度到数据存储
     *
     * @param instanceId     流程实例id
     * @param jobId          流程id
     * @param progress       进度，不能超过100
     * @param completedCount 完成数量
     * @param failCount      失败数量
     * @param skipCount      跳过数量
     * @return 影响条数
     * @throws Exception 失败抛出异常
     */
    int updateJobGraphInstanceProcess(String instanceId, String jobId, int progress, int completedCount,
                                      int failCount, int skipCount) throws Exception;

    /**
     * 更新流程实例对象的调度参数到数据存储
     *
     * @param instanceId 流程实例id
     * @param jobId      流程id
     * @param param      流程实时参数
     * @return 影响条数
     * @throws Exception 失败抛出异常
     */
    int updateJobGraphInstanceParam(String instanceId, String jobId, String param) throws Exception;

    /**
     * 根据流程实例id和流程id 从数据存储中装载全部的任务实例
     *
     * @param jobInstanceId 流程实例id
     * @param jobId         流程id
     * @return 任务实例集合
     * @throws Exception 失败抛出异常
     */
    List<TaskInstance> loadTaskInstance(String jobInstanceId, String jobId) throws Exception;

    /**
     * 装载任务实例从数据存储中，主要是为了重新运行任务实例
     *
     * @param instanceId 任务实例id
     * @param jobId      流程id
     * @param taskId     任务id
     * @return 任务实例对象
     * @throws Exception 失败抛出异常
     */
    TaskInstance loadTaskInstance(String instanceId, String jobId, String taskId) throws Exception;

    /**
     * 插入调度流程实例对象到数据存储，这时的任务可能还没被调度，调度开始和结束时间都是空值
     *
     * @param instances 任务实例id
     * @return 影响条数
     * @throws Exception 失败抛出异常
     */
    int insertTaskInstance(List<TaskInstance> instances) throws Exception;

    /**
     * 修改任务实例状态到数据存储
     *
     * @param instanceId    任务实例ud
     * @param taskId        任务id
     * @param state         状态
     * @param process       进度
     * @param startTime     开始时间
     * @param endTime       结束时间
     * @param exceptionInfo 异常信息
     * @return 影响条数
     * @throws Exception 失败抛出异常
     */
    int updateTaskInstanceState(String instanceId, String taskId, TaskState state, int process,
                                long startTime, Long endTime, String exceptionInfo) throws Exception;

    /**
     * 更新任务实例对象的调度输入或输出参数到数据存储
     *
     * @param instanceId   任务实例id
     * @param taskId       任务id
     * @param param        输入或输出参数
     * @param isInputParam true-表示输入参数，false-表示输出参数
     * @return 影响条数
     * @throws Exception 失败抛出异常
     */
    int updateTaskInstanceState(String instanceId, String taskId, String param, boolean isInputParam) throws Exception;

}
