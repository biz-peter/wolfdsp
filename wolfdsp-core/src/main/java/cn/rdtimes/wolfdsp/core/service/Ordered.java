package cn.rdtimes.wolfdsp.core.service;

/**
 * 优先级接口
 * 具有优先级需要的类或接口需要实现或继承此接口
 *
 * @author BZ
 */
public interface Ordered {
    /**
     * 最高优先级
     */
    int ORDER_HIGHEST = Integer.MIN_VALUE;
    /**
     * 最低优先级
     */
    int ORDER_LOWEST = Integer.MAX_VALUE;
    /**
     * 正常优先级
     */
    int ORDER_NORMAL = Integer.MAX_VALUE >> 1;

    /**
     * @return 返回优先级
     */
    int order();

}
