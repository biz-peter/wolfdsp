package cn.rdtimes.wolfdsp.core.util;

/**
 * 字符串工具类
 *
 * @author BZ
 */
public final class StringUtil {

    public static boolean isEmpty(String s) {
        if (s == null || s.length() == 0) return true;
        return false;
    }

    private StringUtil() {
    }
}
