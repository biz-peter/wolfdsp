package cn.rdtimes.wolfdsp.core.data;

/**
 * 任务状态
 *
 * @author BZ
 */
public enum TaskState {
    READY(0, "未开始"),
    PENDING(1, "等待中"),
    RUNNING(2, "运行中"),
    SKIP(3, "自动跳过(节点不能返回这个状态)"),
    FINISHED(4, "正常完成"),
    FAILED(5, "失败或异常"),
    KILLED(6, "已杀死或已停止"),
    ;

    private final int value;
    private final String name;

    TaskState(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public String getName() {
        return name;
    }

    public static TaskState of(int value) {
        for (TaskState ts : values()) {
            if (ts.getValue() == value) {
                return ts;
            }
        }
        return TaskState.FAILED;
    }
}
