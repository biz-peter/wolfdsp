package cn.rdtimes.wolfdsp.core.service;

/**
 * 服务接口基类
 *
 * @author BZ
 */
public interface Service extends Ordered, Comparable<Service> {

    /**
     * 启动服务，不需要实现的采用默认
     *
     * @throws Exception 抛出异常
     */
    default void start() throws Exception {
        //nothing
    }

    /**
     * 停止服务，不需要实现的采用默认
     */
    default void shutdown() {
        //nothing
    }

    /**
     * 数值越小优先级越高
     *
     * @return 返回优先级，默认最低
     */
    @Override
    default int order() {
        return Ordered.ORDER_NORMAL;
    }

    @Override
    default int compareTo(Service o) {
        return (order() - o.order());
    }

    /**
     * @return 返回服务的名称
     */
    String getName();

}
