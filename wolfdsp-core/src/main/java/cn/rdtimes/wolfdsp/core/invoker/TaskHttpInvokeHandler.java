package cn.rdtimes.wolfdsp.core.invoker;

import cn.rdtimes.wolfdsp.core.invoker.http.HttpInvokeObject;

/**
 * 任务http执行处理器
 *
 * @author BZ
 */
public interface TaskHttpInvokeHandler<R> extends TaskInvokeHandler<HttpInvokeObject, R> {
    /**
     * 启动任务请求url
     */
    String REQ_START_TASK_URL = "/dsp/task/start";
    /**
     * 停止任务请求url
     */
    String REQ_STOP_TASK_URL = "/dsp/task/kill";
    /**
     * 查询任务请求url
     */
    String REQ_QUERY_TASK_URL = "/dsp/task/query";
    /**
     * 节点汇报任务进度请求url,由客户端向服务端发起
     */
    String REQ_REPORT_TASK_URL = "/dsp/task/report";

    /**
     * 停止指定任务
     *
     * @param invokeObject 调用对象,这里不能使用微服务名称负载调用,必须指定ip
     * @return 返回对象根据类型
     * @throws Exception 异常
     */
    R invokeStop(HttpInvokeObject invokeObject) throws Exception;

    /**
     * 查询指定的任务
     *
     * @param invokeObject 调用对象,这里不能使用微服务名称负载调用,必须指定ip
     * @return 返回对象根据类型
     * @throws Exception 异常
     */
    R invokeQuery(HttpInvokeObject invokeObject) throws Exception;

}
