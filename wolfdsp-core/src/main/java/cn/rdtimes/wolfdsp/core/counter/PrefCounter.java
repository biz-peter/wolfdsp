package cn.rdtimes.wolfdsp.core.counter;

import java.util.concurrent.atomic.AtomicLong;

/**
 * 计数器
 *
 * @author BZ
 */
public class PrefCounter {
    private final AtomicLong counter = new AtomicLong(0);
    private final String name;

    private PrefCounter(String name) {
        this(name, 0);
    }

    private PrefCounter(String name, long value) {
        this.name = name;
        counter.set(value);
    }

    public static PrefCounter makePrefCounter(String name) {
        return new PrefCounter(name);
    }

    public static PrefCounter makePrefCounter(String name, long value) {
        return new PrefCounter(name, value);
    }

    public String getName() {
        return name;
    }

    public void clean() {
        counter.set(0);
    }

    public void add(long value) {
        counter.addAndGet(value);
    }

    public void set(long value) {
        counter.set(value);
    }

    public long get() {
        return counter.get();
    }

    public void increment() {
        this.add(1L);
    }

    public void addTime(long value) {
        this.add(value);
    }

    public void elapsedTimeFrom(long value) {
        this.add(System.nanoTime() - value);
    }

    @Override
    public String toString() {
        return "PrefCounter{" +
                "counter=" + counter +
                ", name='" + name + '\'' +
                '}';
    }
}
