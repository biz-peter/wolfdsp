package cn.rdtimes.wolfdsp.core.invoker;

/**
 * 任务执行处理器接口
 *
 * @param <T> 请求类型
 * @param <R> 响应类型
 * @author BZ
 */
public interface TaskInvokeHandler<T, R> {

    enum InvokeKind {
        HTTP,       // http调用
        JAR,        // jar包执行
        SHELL,      // shell脚本
        ;

        public static InvokeKind of(int value) {
            for (InvokeKind i : values()) {
                if (value == i.ordinal()) return i;
            }
            return HTTP;
        }
    }

    /**
     * @return 返回执行任务类型
     */
    InvokeKind getKind();

    /**
     * @return 返回名称, 应该唯一
     */
    String getName();

    /**
     * 执行指定的任务
     * 注意:如果是http请求到某个节点时,要将这个节点的Ip和Port返回.
     * 如果使用map是,key=_realNodeIp表示IP,key=_realNodePort表示port
     *
     * @param invokeObject 调用对象
     * @return 返回对象根据类型
     * @throws Exception 如果处理异常抛出
     */
    R invoke(T invokeObject) throws Exception;

}
