package cn.rdtimes.wolfdsp.core.ha;

import cn.rdtimes.wolfdsp.core.service.Service;

/**
 * 调度平台自身主备策略
 */
public interface PlatformHAStrategy extends Service {
    /**
     * 用于改变ha状态的请求url
     */
    String REQ_CHANGE_HA_STATE_URL = "/dsp/state/change";

    default String getName() {
        return "PlatformHAStrategy";
    }

    enum State {
        MASTER, // 主,可以工作
        STANDBY,// 备,不能工作.只能转换后才能工作
        ;

        public static State of(int s) {
            switch (s) {
                case 0:
                    return MASTER;
                case 1:
                    return STANDBY;
            }
            return STANDBY;
        }
    }

    /**
     * 改变服务器的状态，这可能涉及到重新调度任务等内容
     *
     * @param state 状态
     */
    void changeState(State state);

    /**
     * 返回本机调度主备状态
     *
     * @return MASTER状态才可执行任务
     */
    State getState();

    default boolean enabledPerform() {
        State s = getState();
        return s == State.MASTER;
    }

}
