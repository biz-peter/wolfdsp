package cn.rdtimes.wolfdsp.core.job.params;

import cn.rdtimes.wolfdsp.core.util.StringUtil;
import com.alibaba.fastjson.JSON;

import java.io.Serializable;
import java.util.*;

/**
 * 缺省实现
 *
 * @author BZ
 */
public class DefaultParameters extends AbstractMap<String, Object> implements Parameters<String, Object>, Serializable {
    private static final long serialVersionUID = -1;

    private final Map<String, Object> paramMap;
    private final ParameterKind kind;

    public DefaultParameters(ParameterKind kind) {
        this.paramMap = new HashMap<>();
        this.kind = kind;
    }

    @Override
    public ParameterKind getParameterKind() {
        return kind;
    }

    @Override
    public int size() {
        return paramMap.size();
    }

    @Override
    public boolean containsValue(Object value) {
        return paramMap.containsValue(value);
    }

    @Override
    public boolean containsKey(Object key) {
        return paramMap.containsKey(key);
    }

    @Override
    public Object get(Object key) {
        return paramMap.get(key);
    }

    @Override
    public Object put(String key, Object value) {
        return paramMap.put(key, value);
    }

    @Override
    public void putAll(Map<? extends String, ? extends Object> m) {
        paramMap.putAll(m);
    }

    @Override
    public void clear() {
        paramMap.clear();
    }

    @Override
    public Collection<Object> values() {
        return paramMap.values();
    }

    @Override
    public Set<String> keySet() {
        return paramMap.keySet();
    }

    @Override
    public Set<Entry<String, Object>> entrySet() {
        return paramMap.entrySet();
    }

    @Override
    public Object getOrDefault(Object key, Object defaultValue) {
        return paramMap.getOrDefault(key, defaultValue);
    }

    @Override
    public Object putIfAbsent(String key, Object value) {
        return paramMap.putIfAbsent(key, value);
    }

    @Override
    public Object remove(Object key) {
        return paramMap.remove(key, values());
    }

    @Override
    public boolean replace(String key, Object oldValue, Object newValue) {
        return paramMap.replace(key, oldValue, newValue);
    }

    @Override
    public Object replace(String key, Object value) {
        return paramMap.replace(key, value);
    }

    @Override
    public String toJson() {
        if (paramMap.size() > 0) {
            return JSON.toJSONString(paramMap);
        }
        return null;
    }

    @Override
    public void parseJson(String json) {
        if (!StringUtil.isEmpty(json)) {
            paramMap.putAll(JSON.parseObject(json));
        }
    }

}
