package cn.rdtimes.wolfdsp.core.job.params;

import java.util.Map;

/**
 * 输入输出参数
 *
 * @param <K> key类型
 * @param <V> value类型
 * @author BZ
 */
public interface Parameters<K, V> extends Map<K, V> {

    // 参数类型
    enum ParameterKind {
        JOB_GRAPH,      // 任务流程
        TASK,           // 任务
        ;
    }

    /**
     * @return 返回参数类型
     */
    ParameterKind getParameterKind();

    /**
     * @return 将键值对集合转换成json字符串
     */
    String toJson();

    /**
     * 将字符串转换成键值对集合
     *
     * @param json json字符串
     */
    void parseJson(String json);

}
