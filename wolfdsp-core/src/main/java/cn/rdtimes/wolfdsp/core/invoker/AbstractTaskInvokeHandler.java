package cn.rdtimes.wolfdsp.core.invoker;

import cn.rdtimes.wolfdsp.core.util.StringUtil;

/**
 * 抽象实现, 子类需要继承
 *
 * @param <T> 请求类型
 * @param <R> 响应类型
 * @author BZ
 */
public abstract class AbstractTaskInvokeHandler<T, R> implements TaskInvokeHandler<T, R> {
    private final InvokeKind kind;
    private final String name;

    public AbstractTaskInvokeHandler(InvokeKind kind, String name) {
        this.kind = kind;
        this.name = name;
        TaskInvokeHandlerFactory.registry(this);
    }

    public InvokeKind getKind() {
        return kind;
    }

    public String getName() {
        return name;
    }

    final String[] parseCommandLine(String line) {
        if (StringUtil.isEmpty(line)) {
            throw new NullPointerException("line is null");
        }
        return line.split(" ");
    }

    /**
     * 本地调用响应消息
     */
    public static class LocalResponse {
        public int exitCode;
        public String exceptionInfo;

        public LocalResponse() {
        }

        public LocalResponse(int exitCode, String exceptionInfo) {
            this.exitCode = exitCode;
            this.exceptionInfo = exceptionInfo;
        }
    }

}
