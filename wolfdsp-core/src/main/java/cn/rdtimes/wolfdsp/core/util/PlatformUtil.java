package cn.rdtimes.wolfdsp.core.util;

import sun.misc.Unsafe;

import java.lang.reflect.Field;
import java.security.AccessController;
import java.security.PrivilegedAction;

/**
 * @author BZ
 */
public final class PlatformUtil {
    static final Unsafe UNSAFE;

    static {
        final Object retUnsafe = AccessController.doPrivileged(new PrivilegedAction<Object>() {
            @Override
            public Object run() {
                try {
                    final Field field = Unsafe.class.getDeclaredField("theUnsafe");
                    field.setAccessible(true);
                    return field.get(null);
                } catch (Exception e) {
                    return e;
                }
            }
        });
        if (retUnsafe instanceof Throwable) {
            UNSAFE = null;
        } else {
            UNSAFE = (Unsafe)retUnsafe;
        }
    }

    public static Unsafe getUnsafe() {
        return UNSAFE;
    }

    private PlatformUtil() {}
}
