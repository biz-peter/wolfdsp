package cn.rdtimes.wolfdsp.core.invoker;

/**
 * 缺省实现jar本地执行
 *
 * @author BZ
 */
class DefaultJarTaskInvokeHandler extends AbstractTaskInvokeHandler<String, AbstractTaskInvokeHandler.LocalResponse> {

    DefaultJarTaskInvokeHandler() {
        super(InvokeKind.JAR, "DefaultJarTaskInvokeHandler");
    }

    @Override
    public LocalResponse invoke(String invokeObject) throws Exception {
        String[] cmds = parseCommandLine(invokeObject);
        String[] javaCmds = new String[cmds.length + 2];
        javaCmds[0] = "java";
        javaCmds[1] = "-jar";
        System.arraycopy(cmds, 0, javaCmds, 2, cmds.length);
        ShellCommand command = ShellCommand.builder().setCommands(javaCmds).build();
        StringBuilder sb = command.execute();
        return new LocalResponse(command.exitCode(), sb.toString());
    }

}
