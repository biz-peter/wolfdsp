package cn.rdtimes.wolfdsp.core.data;

/**
 * 调用微服务负载策略
 *
 * @author BZ
 */
public enum BalanceStrategy {
    FIRST,          // 首个
    LAST,           // 最后一个
    ROUND_ROBIN,    // 轮询
    RANDOM,         // 随机
    ;

    public static BalanceStrategy of(int value) {
        for (BalanceStrategy bs : values()) {
            if (bs.ordinal() == value)
                return bs;
        }
        return BalanceStrategy.FIRST;
    }

}
