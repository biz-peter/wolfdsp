package cn.rdtimes.wolfdsp.core.ha;

import cn.rdtimes.wolfdsp.core.service.Service;

import java.util.List;

/**
 * 微服务名称服务
 *
 * @author BZ
 */
public interface NameService extends Service {
    // 微服务地址信息
    class ServerInfo {
        public String serviceName;     // 微服务名称
        public String ip;       // ip地址
        public int port;        // 端口
        public boolean isTls;   // 是否tls加密,true加密
    }

    default String getName() {
        return "NameService";
    }

    /**
     * 根据微服务名称返回服务器列表
     *
     * @param serviceName 微服务名称
     * @return 服务器列表
     */
    List<ServerInfo> getServerInfo(String serviceName);

}
