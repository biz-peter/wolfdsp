package cn.rdtimes.wolfdsp.core.data;

import java.io.Serializable;

/**
 * 调度实例对象，用于调度任务流程
 *
 * @author BZ
 */
public class ScheduledInstance implements Serializable {
    private static final long serialVersionUID = -1;

    /**********下面字段可以动态更改**********/
    // 开始调度时间
    private volatile long startTime;
    // 下次调度开始时间，空值说明只调度一次就结束
    // 单位：毫秒
    private volatile Long nextScheduleTime;

    /*********下面是继承JobGraphDefinition**************/
    private String cronExp;
    // 唯一标识
    private String jobId;
    // 名称
    private String jobName;

    private boolean enabledContinue;
    // 流程参数json字符串，格式：{"xxx":"yyy","xxx1":"yyy1"...}
    private String paramJson;

    public ScheduledInstance() {
    }

    public ScheduledInstance(JobGraphDefinition definition) {
        this.cronExp = definition.getCronExp();
        this.jobId = definition.getJobId();
        this.jobName = definition.getJobName();
        this.enabledContinue = definition.isEnabledContinue();
        this.paramJson = definition.getParamJson();
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public Long getNextScheduleTime() {
        return nextScheduleTime;
    }

    public void setNextScheduleTime(Long nextScheduleTime) {
        this.nextScheduleTime = nextScheduleTime;
    }

    public String getCronExp() {
        return cronExp;
    }

    public void setCronExp(String cronExp) {
        this.cronExp = cronExp;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public boolean isEnabledContinue() {
        return enabledContinue;
    }

    public void setEnabledContinue(boolean enabledContinue) {
        this.enabledContinue = enabledContinue;
    }

    public String getParamJson() {
        return paramJson;
    }

    public void setParamJson(String paramJson) {
        this.paramJson = paramJson;
    }

    @Override
    public String toString() {
        return "ScheduledInstance{" +
                ", startTime=" + startTime +
                ", nextScheduleTime=" + nextScheduleTime +
                ", cronExp='" + cronExp + '\'' +
                ", jobId='" + jobId + '\'' +
                ", jobName='" + jobName + '\'' +
                ", enabledContinue=" + enabledContinue +
                ", paramJson='" + paramJson + '\'' +
                '}';
    }

}
