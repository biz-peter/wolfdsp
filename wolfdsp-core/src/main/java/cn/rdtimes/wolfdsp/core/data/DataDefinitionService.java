package cn.rdtimes.wolfdsp.core.data;

import cn.rdtimes.wolfdsp.core.service.Service;

import java.util.List;

/**
 * 数据定义服务
 * 这里数据对象与数据存储直接相关，实际存储数据按需扩展
 *
 * @author BZ
 */
public interface DataDefinitionService extends Service {

    default String getName() {
        return "DataDefinitionService";
    }

    /**
     * 从数据存储中装载指定的任务流程定义
     *
     * @param jobId 任务流程id
     * @return 调度流程对象
     * @throws Exception 数据存储异常
     */
    JobGraphDefinition loadJobGraphDefinition(String jobId) throws Exception;

    /**
     * 获取调度实例对象
     *
     * @param definition 任务流程定义对象
     * @return 调度实例
     */
    default ScheduledInstance getScheduledInstance(JobGraphDefinition definition) {
        ScheduledInstance scheduledInstance = new ScheduledInstance(definition);
        scheduledInstance.setStartTime(System.currentTimeMillis());
        return scheduledInstance;
    }

    /**
     * 从数据存储中装载指定任务流程id下的所有任务
     *
     * @param jobId 任务流程id
     * @return 任务集合
     * @throws Exception 数据存储异常
     */
    List<TaskDefinition> loadTaskDefinition(String jobId) throws Exception;

}
