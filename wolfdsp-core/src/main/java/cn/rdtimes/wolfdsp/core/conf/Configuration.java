package cn.rdtimes.wolfdsp.core.conf;

import cn.rdtimes.wolfdsp.core.ha.PlatformHAStrategy;
import cn.rdtimes.wolfdsp.core.util.StringUtil;

import java.util.*;

/**
 * 平台参数配置
 *
 * @author BZ
 */
public class Configuration extends AbstractMap<String, Object> {

    private final HashMap<String, Object> internalMap;

    public Configuration() {
        internalMap = new HashMap<>();
    }

    // 任务流程最大并发数量
    public int getJobGraphRunningCount() {
        Object obj = internalMap.get("jobGraphRunningCount");
        return (obj == null ? Integer.MAX_VALUE : (int) obj);
    }

    public void setJobGraphRunningCount(int count) {
        if (count <= 0) return;
        internalMap.put("jobGraphRunningCount", count);
    }

    // 任务状态查询间隔时间，单位 ： 毫秒
    public int getTaskQueryInterval() {
        Object obj = internalMap.get("taskQueryInterval");
        return (obj == null ? 10_000 : (int) obj);
    }

    public void setTaskQueryInterval(int interval) {
        if (interval <= 0) return;
        internalMap.put("taskQueryInterval", interval);
    }

    // 调度平台主地址或本服务地址
    public String getMasterIpAndPort() {
        Object obj = internalMap.get("masterIp");
        return (obj == null ? null : obj.toString());
    }

    public void setMasterIpAndPort(String ipAndPort) {
        if (StringUtil.isEmpty(ipAndPort)) {
            throw new NullPointerException("isAndPort is null");
        }
        internalMap.put("masterIp", ipAndPort);
    }

    // 调度平台从地址
    public String getStandbyIpAndPort() {
        Object obj = internalMap.get("standbyIp");
        return (obj == null ? null : obj.toString());
    }

    public void setStandbyIpAndPort(String ipAndPort) {
        if (StringUtil.isEmpty(ipAndPort)) return;
        internalMap.put("standby", ipAndPort);
    }

    // 获取指定的http调用处理器id，否则使用默认的
    public String getHttpTaskInvokeHandlerId() {
        Object obj = internalMap.get("httpTaskInvokeHandlerIp");
        return (obj == null ? null : obj.toString());
    }

    public void setHttpTaskInvokeHandlerId(String id) {
        if (StringUtil.isEmpty(id)) return;
        internalMap.put("httpTaskInvokeHandlerIp", id);
    }

    // 获取指定的jar调用处理器id， 否则使用默认的
    public String getJarTaskInvokeHandlerId() {
        Object obj = internalMap.get("jarTaskInvokeHandlerId");
        return (obj == null ? null : obj.toString());
    }

    public void setJarTaskInvokeHandlerId(String id) {
        if (StringUtil.isEmpty(id)) return;
        internalMap.put("jarTaskInvokeHandlerId", id);
    }

    // 获取指定的Shell调用处理器id，否则使用默认的
    public String getShellTaskInvokeHandlerId() {
        Object obj = internalMap.get("shellTaskInvokeHandlerId");
        return (obj == null ? null : obj.toString());
    }

    public void setShellTaskInvokeHandlerId(String id) {
        if (StringUtil.isEmpty(id)) return;
        internalMap.put("shellTaskInvokeHandlerId", id);
    }

    // 调度线程数
    public int getScheduleThreadCount() {
        Object obj = internalMap.get("scheduleThreadCount");
        return (obj == null ? 2 : (int) obj);
    }

    public void setScheduleThreadCount(int count) {
        if (count <= 0) return;
        internalMap.put("scheduleThreadCount", count);
    }

    // 任务执行线程数
    public int getTaskThreadCount() {
        Object obj = internalMap.get("taskThreadCount");
        return (obj == null ? 5 : (int) obj);
    }

    public void setTaskThreadCount(int count) {
        if (count <= 0) return;
        internalMap.put("taskThreadCount", count);
    }

    //ha 开始状态
    public PlatformHAStrategy.State getHAStrategyState() {
        Object obj = internalMap.get("platformHAStrategyState");
        return (obj == null ? PlatformHAStrategy.State.STANDBY : (PlatformHAStrategy.State) obj);
    }

    public void setHAStrategyState(int s) {
        PlatformHAStrategy.State state = PlatformHAStrategy.State.of(s);
        internalMap.put("platformHAStrategyState", state);
    }

    /*********************
     * 下面是继承map的方法
     * *******************/

    @Override
    public int size() {
        return internalMap.size();
    }

    @Override
    public boolean containsValue(Object value) {
        return internalMap.containsValue(value);
    }

    @Override
    public boolean containsKey(Object key) {
        return internalMap.containsKey(key);
    }

    @Override
    public Object get(Object key) {
        return internalMap.get(key);
    }

    @Override
    public Object put(String key, Object value) {
        return internalMap.put(key, value);
    }

    @Override
    public void putAll(Map<? extends String, ? extends Object> m) {
        internalMap.putAll(m);
    }

    @Override
    public void clear() {
        internalMap.clear();
    }

    @Override
    public Collection<Object> values() {
        return internalMap.values();
    }

    @Override
    public Set<String> keySet() {
        return internalMap.keySet();
    }

    @Override
    public Set<Entry<String, Object>> entrySet() {
        return internalMap.entrySet();
    }

    @Override
    public Object getOrDefault(Object key, Object defaultValue) {
        return internalMap.getOrDefault(key, defaultValue);
    }

    @Override
    public Object putIfAbsent(String key, Object value) {
        return internalMap.putIfAbsent(key, value);
    }

    @Override
    public Object remove(Object key) {
        return internalMap.remove(key, values());
    }

    @Override
    public boolean replace(String key, Object oldValue, Object newValue) {
        return internalMap.replace(key, oldValue, newValue);
    }

    @Override
    public Object replace(String key, Object value) {
        return internalMap.replace(key, value);
    }
}

