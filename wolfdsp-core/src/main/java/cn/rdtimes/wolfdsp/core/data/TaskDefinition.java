package cn.rdtimes.wolfdsp.core.data;

import cn.rdtimes.wolfdsp.core.invoker.TaskInvokeHandler;

import java.io.Serializable;

/**
 * 任务定义
 *
 * @author BZ
 */
public class TaskDefinition implements Serializable {
    private static final long serialVersionUID = -1;

    // 任务流程id
    private String jobId;
    // 任务id,唯一
    private String taskId;
    // 所有前置任务id,多个逗号分隔,如:1,2,3
    private String preTaskIds;
    // 任务名称
    private String taskName;
    // 输入参数json字符串
    private String inputParamJson;
    // 输出参数json字符串
    private String outputParamJson;
    //调度类型
    private TaskInvokeHandler.InvokeKind invokeKind = TaskInvokeHandler.InvokeKind.HTTP;
    // 命令行字符串,给出脚本文件的全限文件名,参数用空格分开(/home/test.sh -a -b)
    // 如果是jar包也要给出jar具体全限文件名,jar包必须是能直接执行的 java -jar
    // 如果是http请求,是空值
    private String commandLine;
    // ip地址
    private String ip;
    // 端口
    private int port;
    //是否可以在失败或异常是重新执行
    private boolean enabledRepeat;
    // 失败或异常时是否能自动跳过
    private boolean autoSkip;
    // 微服务名称
    private String serviceName;
    // 微服务名称使用的负载策略
    private BalanceStrategy balanceStrategy = BalanceStrategy.FIRST;

    public TaskDefinition() {
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getPreTaskIds() {
        return preTaskIds;
    }

    public void setPreTaskIds(String preTaskIds) {
        this.preTaskIds = preTaskIds;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getInputParamJson() {
        return inputParamJson;
    }

    public void setInputParamJson(String inputParamJson) {
        this.inputParamJson = inputParamJson;
    }

    public String getOutputParamJson() {
        return outputParamJson;
    }

    public void setOutputParamJson(String outputParamJson) {
        this.outputParamJson = outputParamJson;
    }

    public TaskInvokeHandler.InvokeKind getInvokeKind() {
        return invokeKind;
    }

    public void setInvokeKind(TaskInvokeHandler.InvokeKind invokeKind) {
        this.invokeKind = invokeKind;
    }

    public String getCommandLine() {
        return commandLine;
    }

    public void setCommandLine(String commandLine) {
        this.commandLine = commandLine;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public boolean isEnabledRepeat() {
        return enabledRepeat;
    }

    public void setEnabledRepeat(boolean enabledRepeat) {
        this.enabledRepeat = enabledRepeat;
    }

    public boolean isAutoSkip() {
        return autoSkip;
    }

    public void setAutoSkip(boolean autoSkip) {
        this.autoSkip = autoSkip;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public BalanceStrategy getBalanceStrategy() {
        return balanceStrategy;
    }

    public void setBalanceStrategy(BalanceStrategy balanceStrategy) {
        this.balanceStrategy = balanceStrategy;
    }

    @Override
    public String toString() {
        return "TaskDefinition{" +
                "jobId='" + jobId + '\'' +
                ", taskId='" + taskId + '\'' +
                ", perTaskIds='" + preTaskIds + '\'' +
                ", taskName='" + taskName + '\'' +
                ", inputParamJson='" + inputParamJson + '\'' +
                ", outputParamJson='" + outputParamJson + '\'' +
                ", invokeKind=" + invokeKind +
                ", commandLine='" + commandLine + '\'' +
                ", ip='" + ip + '\'' +
                ", port=" + port +
                ", enabledRepeat=" + enabledRepeat +
                ", autoSkip=" + autoSkip +
                ", serviceName='" + serviceName + '\'' +
                ", balanceStrategy=" + balanceStrategy +
                '}';
    }
}
