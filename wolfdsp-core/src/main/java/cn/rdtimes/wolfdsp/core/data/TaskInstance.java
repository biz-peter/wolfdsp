package cn.rdtimes.wolfdsp.core.data;

import cn.rdtimes.wolfdsp.core.invoker.TaskInvokeHandler;

import java.io.Serializable;

/**
 * 任务实例
 *
 * @author BZ
 */
public class TaskInstance implements Serializable {
    private static final long serialVersionUID = -1;

    /************以下字段是继承TaskDefinition***************/
    // 任务id,唯一
    private String taskId;
    // job实例id
    private String jobInstanceId;
    // 流程id
    private String jobId;
    // 所有前置任务id,多个逗号分隔,如:1,2,3
    private String preTaskIds;
    // 任务名称
    private String taskName;
    // 输入参数json字符串
    private String inputParamJson;
    // 输出参数json字符串
    private String outputParamJson;
    //调度类型
    private TaskInvokeHandler.InvokeKind invokeKind = TaskInvokeHandler.InvokeKind.HTTP;
    // 命令行字符串,给出脚本文件的全限文件名,参数用空格分开(/home/test.sh -a -b)
    // 如果是jar包也要给出jar具体全限文件名,jar包必须是能直接执行的 java -jar
    // 如果是http请求,是空值
    private String commandLine;
    // ip地址
    private String ip;
    // 端口
    private int port;
    //是否可以在失败或异常是重新执行
    private boolean enabledRepeat;
    // 失败或异常时是否能自动跳过
    private boolean autoSkip;
    // 微服务名称
    private String serviceName;
    // 微服务名称使用的负载策略
    private BalanceStrategy balanceStrategy = BalanceStrategy.FIRST;

    /**************以下字段是可以动态变化的****************/
    // 实例id
    private String instanceId;
    // 任务状态
    private volatile TaskState taskState = TaskState.READY;
    // 执行进度,100%方式
    private volatile int progress;
    // 调度开始时间
    private volatile long startTime;
    // 调度结束时间
    private volatile Long endTime;
    // 异常信息
    private volatile String exceptionInfo;

    public TaskInstance() {
    }

    public void clean() {
        progress = 0;
        endTime = null;
        exceptionInfo = null;
    }

    public void set(TaskDefinition def) {
        this.jobId = def.getJobId();
        this.taskId = def.getTaskId();
        this.preTaskIds = def.getPreTaskIds();
        this.taskName = def.getTaskName();
        this.inputParamJson = def.getInputParamJson();
        this.outputParamJson = def.getOutputParamJson();
        this.invokeKind = def.getInvokeKind();
        this.commandLine = def.getCommandLine();
        this.ip = def.getIp();
        this.port = def.getPort();
        this.enabledRepeat = def.isEnabledRepeat();
        this.autoSkip = def.isAutoSkip();
        this.serviceName = def.getServiceName();
        this.balanceStrategy = def.getBalanceStrategy();
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getJobInstanceId() {
        return jobInstanceId;
    }

    public void setJobInstanceId(String jobInstanceId) {
        this.jobInstanceId = jobInstanceId;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public String getPreTaskIds() {
        return preTaskIds;
    }

    public void setPreTaskIds(String preTaskIds) {
        this.preTaskIds = preTaskIds;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getInputParamJson() {
        return inputParamJson;
    }

    public void setInputParamJson(String inputParamJson) {
        this.inputParamJson = inputParamJson;
    }

    public String getOutputParamJson() {
        return outputParamJson;
    }

    public void setOutputParamJson(String outputParamJson) {
        this.outputParamJson = outputParamJson;
    }

    public TaskInvokeHandler.InvokeKind getInvokeKind() {
        return invokeKind;
    }

    public void setInvokeKind(TaskInvokeHandler.InvokeKind invokeKind) {
        this.invokeKind = invokeKind;
    }

    public String getCommandLine() {
        return commandLine;
    }

    public void setCommandLine(String commandLine) {
        this.commandLine = commandLine;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public boolean isEnabledRepeat() {
        return enabledRepeat;
    }

    public void setEnabledRepeat(boolean enabledRepeat) {
        this.enabledRepeat = enabledRepeat;
    }

    public boolean isAutoSkip() {
        return autoSkip;
    }

    public void setAutoSkip(boolean autoSkip) {
        this.autoSkip = autoSkip;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public BalanceStrategy getBalanceStrategy() {
        return balanceStrategy;
    }

    public void setBalanceStrategy(BalanceStrategy balanceStrategy) {
        this.balanceStrategy = balanceStrategy;
    }

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

    public TaskState getTaskState() {
        return taskState;
    }

    public void setTaskState(TaskState taskState) {
        this.taskState = taskState;
    }

    public int getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress = progress;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public String getExceptionInfo() {
        return exceptionInfo;
    }

    public void setExceptionInfo(String exceptionInfo) {
        this.exceptionInfo = exceptionInfo;
    }

    @Override
    public String toString() {
        return "TaskInstance{" +
                "taskId='" + taskId + '\'' +
                ", jobInstanceId='" + jobInstanceId + '\'' +
                ", jobId='" + jobId + '\'' +
                ", preTaskIds='" + preTaskIds + '\'' +
                ", taskName='" + taskName + '\'' +
                ", inputParamJson='" + inputParamJson + '\'' +
                ", outputParamJson='" + outputParamJson + '\'' +
                ", invokeKind=" + invokeKind +
                ", commandLine='" + commandLine + '\'' +
                ", ip='" + ip + '\'' +
                ", port=" + port +
                ", enabledRepeat=" + enabledRepeat +
                ", autoSkip=" + autoSkip +
                ", serviceName='" + serviceName + '\'' +
                ", balanceStrategy=" + balanceStrategy +
                ", instanceId='" + instanceId + '\'' +
                ", taskState=" + taskState +
                ", progress=" + progress +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", exceptionInfo='" + exceptionInfo + '\'' +
                '}';
    }
}
