package cn.rdtimes.wolfdsp.core.data;

import java.io.Serializable;

/**
 * 任务流程对象
 *
 * @author BZ
 */
public class JobGraphInstance implements Serializable {
    private static final long serialVersionUID = -1;

    /********以下来自JobGraphDefinition*********/
    // 唯一标识
    private String jobId;
    // 名称
    private String jobName;
    // 是否可以在失败或异常处继续执行
    private boolean enabledContinue;
    // 流程参数json字符串，格式：{"xxx":"yyy","xxx1":"yyy1"...}
    private String paramJson;

    /************以下字段可以动态更改*************/
    // 实例id
    private String instanceId;
    // 调度开始时间
    private volatile long startTime;
    // 调度结束时间
    private volatile Long endTime;
    // 调度状态
    private volatile JobGraphState jobGraphState = JobGraphState.UNSCHEDULED;
    // 异常信息
    private volatile String exceptionInfo;
    // 调度执行进度。100%方式
    private volatile int progress;
    // 完成数量
    private int completedCount;
    // 失败数量
    private int failCount;
    // 跳过数量
    private int skipCount;

    public JobGraphInstance() {
    }

    public void set(ScheduledInstance instance) {
        this.jobId = instance.getJobId();
        this.jobName = instance.getJobName();
        this.enabledContinue = instance.isEnabledContinue();
        this.paramJson = instance.getParamJson();
    }

    public void incCompletedCount() {
        ++completedCount;
    }

    public void incFailCount() {
        ++failCount;
    }

    public void incSkipCount() {
        ++skipCount;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public boolean isEnabledContinue() {
        return enabledContinue;
    }

    public void setEnabledContinue(boolean enabledContinue) {
        this.enabledContinue = enabledContinue;
    }

    public String getParamJson() {
        return paramJson;
    }

    public void setParamJson(String paramJson) {
        this.paramJson = paramJson;
    }

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public JobGraphState getJobGraphState() {
        return jobGraphState;
    }

    public void setJobGraphState(JobGraphState jobGraphState) {
        this.jobGraphState = jobGraphState;
    }

    public String getExceptionInfo() {
        return exceptionInfo;
    }

    public void setExceptionInfo(String exceptionInfo) {
        this.exceptionInfo = exceptionInfo;
    }

    public int getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress = progress;
    }

    public int getCompletedCount() {
        return completedCount;
    }

    public void setCompletedCount(int completedCount) {
        this.completedCount = completedCount;
    }

    public int getFailCount() {
        return failCount;
    }

    public void setFailCount(int failCount) {
        this.failCount = failCount;
    }

    public int getSkipCount() {
        return skipCount;
    }

    public void setSkipCount(int skipCount) {
        this.skipCount = skipCount;
    }

    public void clean() {
        exceptionInfo = null;
        endTime = null;
        progress = completedCount = skipCount = failCount = 0;
    }

    @Override
    public String toString() {
        return "JobGraphInstance{" +
                "jobId='" + jobId + '\'' +
                ", jobName='" + jobName + '\'' +
                ", enabledContinue=" + enabledContinue +
                ", paramJson='" + paramJson + '\'' +
                ", instanceId='" + instanceId + '\'' +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", jobGraphState=" + jobGraphState +
                ", exceptionInfo='" + exceptionInfo + '\'' +
                ", progress=" + progress +
                ", completedCount=" + completedCount +
                ", failCount=" + failCount +
                ", skipCount=" + skipCount +
                '}';
    }
}
