package cn.rdtimes.wolfdsp.core.ha;

import cn.rdtimes.wolfdsp.core.service.Service;

import java.io.IOException;
import java.io.Serializable;
import java.util.Collection;

/**
 * 状态数据同步实现思路：
 * 1）传输相关变化对象的Id，不进行序列化对象数据传输
 * 2）备份变化的Id
 * 3）当转换为Master的时候，JobGraphService装载调度任务
 */

public interface StateSyncService extends Service {
    /**
     * 用于同步数据状态的请求url
     */
    String REQ_SYN_STATE_URL = "/dsp/state/sync";

    default String getName() {
        return "StateSyncService";
    }

    /**
     * 发送状态数据到备服务器
     *
     * @param stateDTO 状态数据
     * @throws IOException 网络异常
     */
    void sendState(StateDTO stateDTO) throws IOException;

    /**
     * 接收状态数据，将json字符串转换对象
     *
     * @param json json字符串
     * @return 同步对象
     * @throws Exception 解析异常等
     */
    void receiveState(String json) throws Exception;

    /**
     * @return 返回状态数据列表
     */
    Collection<StateDTO> getStateDTOList();

    /**
     * 传输用的状态数据，只传输已调度的任务流程id即可
     */
    class StateDTO implements Serializable {
        private static final long serialVersionUID = -1;

        //添加状态
        public static final int STATE_ADD = 1;
        //删除状态
        public static final int STATE_DEL = 2;

        //状态类型
        private int stateKind;
        //任务流程id
        private String jobId;

        public StateDTO() {
        }

        public static StateDTO of(int stateKind, String jobId) {
            StateDTO dto = new StateDTO();
            dto.setStateKind(stateKind);
            dto.setJobId(jobId);
            return dto;
        }

        public int getStateKind() {
            return stateKind;
        }

        public void setStateKind(int stateKind) {
            this.stateKind = stateKind;
        }

        public String getJobId() {
            return jobId;
        }

        public void setJobId(String jobId) {
            this.jobId = jobId;
        }

        @Override
        public String toString() {
            return "StateDTO{" +
                    "stateKind=" + stateKind +
                    ", jobId='" + jobId + '\'' +
                    '}';
        }
    }

}
