package cn.rdtimes.wolfdsp.core.util;

import java.util.Map;

/**
 * 响应工具
 * "jobId": "job-app-xxxx"
 * "taskId": "task-app-xxxx"
 * "haveScheduleTask": "true-有,false-无(是否有此调度任务，有可能调度完成时已删除，查询请求晚于这个时间，所以服务端要处理这种情况)"
 * "state": 参见TaskState
 * "timestamp": 2123344444444
 * "exception": "异常信息"
 * "curProgress": 20(当前进度，百分比)
 * "outputParams": { // 可选，如果是完成并且有输出就需要输出了（不一定全部参数）
 * "key": "value",
 * "key1": "value1"
 * ...
 * }
 *
 * @author BZ
 */
public final class ResponseUtil {

    public static String getNodeIp(Map<String, Object> params, String defaultValue) {
        return getValue(params, "_realNodeIp", defaultValue);
    }

    public static int getNodePort(Map<String, Object> params, int defaultValue) {
        return getValue(params, "_realNodePort", defaultValue);
    }

    public static String getJobId(Map<String, Object> params, String defaultValue) {
        return getValue(params, "jobId", defaultValue);
    }

    public static String getTaskId(Map<String, Object> params, String defaultValue) {
        return getValue(params, "taskId", defaultValue);
    }

    public static boolean getHaveScheduleTask(Map<String, Object> params, boolean defaultValue) {
        return getValue(params, "haveScheduleTask", defaultValue);
    }

    public static int getState(Map<String, Object> params, int defaultValue) {
        return getValue(params, "state", defaultValue);
    }

    public static long getTimestamp(Map<String, Object> params, int defaultValue) {
        return getValue(params, "timestamp", defaultValue);
    }

    public static String getException(Map<String, Object> params, String defaultValue) {
        return getValue(params, "exception", defaultValue);
    }

    public static int getCurProgress(Map<String, Object> params, int defaultValue) {
        return getValue(params, "curProgress", defaultValue);
    }

    public static String getOutputParams(Map<String, Object> params, String defaultValue) {
        return getValue(params, "outputParams", defaultValue);
    }

    private static <T> T getValue(Map<String, Object> params, String key, T defaultValue) {
        Object ret = params.get(key);
        return ret == null ? defaultValue : (T) ret;
    }

    private ResponseUtil() {
        // nothing
    }

}
