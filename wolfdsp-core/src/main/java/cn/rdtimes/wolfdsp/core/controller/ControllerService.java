package cn.rdtimes.wolfdsp.core.controller;

import cn.rdtimes.wolfdsp.core.data.JobGraphState;
import cn.rdtimes.wolfdsp.core.data.TaskState;
import cn.rdtimes.wolfdsp.core.ha.PlatformHAStrategy;
import cn.rdtimes.wolfdsp.core.invoker.http.ResponseMessage;
import cn.rdtimes.wolfdsp.core.service.ScheduleManager;

import java.util.Map;

/**
 * 直接提供控制器服务，主要能快速完成客户端请求
 * 这个是一个辅助类
 *
 * @author BZ
 */
public class ControllerService {

    /**
     * 改变HA策略状态，由主到备或由备到主
     *
     * @param state 要改变的状态
     */
    public void changeHAStrategyState(PlatformHAStrategy.State state) {
        ScheduleManager.getInstance().getPlatformHAStrategy().changeState(state);
    }

    /**
     * 接收数据同步状态
     *
     * @param json json字符串
     */
    public void receiveSyncState(String json) throws Exception {
        ScheduleManager.getInstance().getStateSyncService().receiveState(json);
    }

    ////////////下面是JobGraphService的方法调用////////////
    public void schedule(String jobId) throws Exception {
        ScheduleManager.getInstance().getJobGraphService().schedule(jobId);
    }

    public void scheduleContinue(String jobId) throws Exception {
        ScheduleManager.getInstance().getJobGraphService().scheduleContinue(jobId);
    }

    public JobGraphState jobInstanceContinue(String instanceId, String jobId) throws Exception {
        return ScheduleManager.getInstance().getJobGraphService().jobInstanceContinue(instanceId, jobId);
    }

    public JobGraphState killJobInstance(String instanceId, String jobId) throws Exception {
        return ScheduleManager.getInstance().getJobGraphService().killJobInstance(instanceId, jobId);
    }

    public TaskState againRunTask(String instanceId, String jobId, String taskId) throws Exception {
        return ScheduleManager.getInstance().getJobGraphService().againRunTask(instanceId, jobId, taskId);
    }

    public ResponseMessage<?> taskReportRequest(Map<String, Object> request) {
        return ScheduleManager.getInstance().getJobGraphService().taskReportRequest(request);
    }

}
