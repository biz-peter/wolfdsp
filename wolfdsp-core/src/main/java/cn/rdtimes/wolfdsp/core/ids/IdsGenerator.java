package cn.rdtimes.wolfdsp.core.ids;

import cn.rdtimes.wolfdsp.core.service.Service;

/**
 * id各类生成器
 *
 * @author BZ
 */
public interface IdsGenerator extends Service {

    /**
     * 任务流程定义id前缀
     */
    String JOB_DEFINITION_ID_PREF = "job_def_";
    /**
     * 任务流程实例id前缀
     */
    String JOB_INSTANCE_ID_PREF = "job_int_";
    /**
     * 任务定义id前缀
     */
    String TASK_DEFINITION_ID_PRE = "task_def_";
    /**
     * 任务实例id前缀
     */
    String TASK_INSTANCE_ID_PREF = "task_int_";

    default String getName() {
        return "IdsGenerator";
    }

    /**
     * @return 返回调用流程定义id
     */
    String generateJobDefinitionId();

    /**
     * @return 返回调度流程实例id
     */
    String generateJobInstanceId();

    /**
     * @return 返回任务定义id
     */
    String generateTaskDefinitionId();

    /**
     * @return 返回任务实例id
     */
    String generateTaskInstanceId();

}
