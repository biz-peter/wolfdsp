package cn.rdtimes.wolfdsp.sdk;

/**
 * 创建任务工厂,这个需要实现
 *
 * @author BZ
 */
public interface TaskFactory {

    /**
     * 根据流程id 和任务id 获取或创建任务
     *
     * @param meta
     * @return
     */
    Task getTask(TaskStartMeta meta);

}
