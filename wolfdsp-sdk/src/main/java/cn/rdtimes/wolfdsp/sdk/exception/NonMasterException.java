package cn.rdtimes.wolfdsp.sdk.exception;

/**
 * 非主节点异常,这时候要更换备节点在请求
 *
 * @author BZ
 */
public class NonMasterException extends Exception {

    public NonMasterException(String msg) {
        super(msg);
    }

}
