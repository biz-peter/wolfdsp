package cn.rdtimes.wolfdsp.sdk.util;

/**
 * 字符串工具类
 *
 * @author BZ
 */
public final class StringUtil {

    public static boolean isEmpty(String s) {
        return s == null || s.length() == 0;
    }

    private StringUtil() {
    }

}
