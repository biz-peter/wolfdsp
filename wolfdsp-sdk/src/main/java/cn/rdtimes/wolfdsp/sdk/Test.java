package cn.rdtimes.wolfdsp.sdk;

/**
 * 测试类
 *
 * @author BZ
 */
public class Test {

    public static void test() {
        TaskService taskService = new TaskService(new TaskFactory() {

            @Override
            public Task getTask(TaskStartMeta meta) {
                return null;
            }
        }, 3);

        ControllerService controllerService = new ControllerService();
        controllerService.processStart(null);
        controllerService.processStop(null);
        controllerService.processQueryState(null);
    }

}
