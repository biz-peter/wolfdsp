package cn.rdtimes.wolfdsp.sdk.exception;

/**
 * 响应返回的code代码不是成功的异常
 *
 * @author BZ
 */
public class ResponseCodeException extends Exception {

    public ResponseCodeException(String msg) {
        super(msg);
    }

}
