package cn.rdtimes.wolfdsp.sdk.util;

import com.alibaba.fastjson.JSON;

import java.util.Collections;
import java.util.Map;

/**
 * json工具
 *
 * @author BZ
 */
public final class JsonUtil {

    public static Map toMap(String json) {
        if (StringUtil.isEmpty(json))
            return Collections.EMPTY_MAP;
        return JSON.parseObject(json);
    }

    public static String toString(Map<String, Object> map) {
        if (map == null)
            return null;
        return JSON.toJSONString(map);
    }

    private JsonUtil() {
    }

}
