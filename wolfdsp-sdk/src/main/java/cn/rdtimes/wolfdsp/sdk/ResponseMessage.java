package cn.rdtimes.wolfdsp.sdk;

import java.io.Serializable;

/**
 * 响应消息体
 *
 * @author BZ
 */
public class ResponseMessage<T> implements Serializable {
    // 成功
    public static final int SUCC = 0;
    // 不是主服务器
    public static final int NOT_MASTER_SERVER = 19999;
    // error
    public static final int ERROR = 99999;

    public static final ResponseMessage OK = new ResponseMessage(SUCC);
    public static final ResponseMessage ERR = new ResponseMessage(ERROR);

    //响应码
    private int code;
    // 响应消息
    private String msg;
    // 响应数据
    private T data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public ResponseMessage() {
    }

    public ResponseMessage(int code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public ResponseMessage(int code) {
        this(code, null);
    }

    public ResponseMessage(int code, String msg) {
        this(code, msg, null);
    }

    public static <T> ResponseMessage<T> sucess() {
        return success("OK", null);
    }

    public static <T> ResponseMessage<T> success(T data) {
        return success("OK", data);
    }

    public static <T> ResponseMessage<T> success(String msg, T data) {
        return new ResponseMessage<>(SUCC, msg, data);
    }

    public static <T> ResponseMessage<T> error(int code, String msg, T data) {
        return new ResponseMessage<>(code, msg, data);
    }

    public static <T> ResponseMessage<T> error(int code, T data) {
        return error(code, "ERR", data);
    }

    public static <T> ResponseMessage<T> error(int code) {
        return error(code, "ERR", null);
    }

    public static <T> ResponseMessage<T> error(String msg) {
        return error(ERROR, msg, null);
    }

}
