package cn.rdtimes.wolfdsp.sdk;

import java.util.Map;

/**
 * 开始启动任务时的请求元数据
 *
 * @author BZ
 */
public class TaskStartMeta {
    private String jobId;
    private String taskId;
    private String masterIp;
    private int masterPort;
    private String standbyIp;
    private int standbyPort;
    private long timestamp;
    private Map<String, Object> inputParams;

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getMasterIp() {
        return masterIp;
    }

    public void setMasterIp(String masterIp) {
        this.masterIp = masterIp;
    }

    public int getMasterPort() {
        return masterPort;
    }

    public void setMasterPort(int masterPort) {
        this.masterPort = masterPort;
    }

    public String getStandbyIp() {
        return standbyIp;
    }

    public void setStandbyIp(String standbyIp) {
        this.standbyIp = standbyIp;
    }

    public int getStandbyPort() {
        return standbyPort;
    }

    public void setStandbyPort(int standbyPort) {
        this.standbyPort = standbyPort;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public Map<String, Object> getInputParams() {
        return inputParams;
    }

    public void setInputParams(Map<String, Object> inputParams) {
        this.inputParams = inputParams;
    }

    @Override
    public String toString() {
        return "TaskStartMeta{" +
                "jobId='" + jobId + '\'' +
                ", taskId='" + taskId + '\'' +
                ", masterIp='" + masterIp + '\'' +
                ", masterPort=" + masterPort +
                ", standbyIp='" + standbyIp + '\'' +
                ", standbyPort=" + standbyPort +
                ", timestamp=" + timestamp +
                ", inputParams=" + inputParams +
                '}';
    }

}
