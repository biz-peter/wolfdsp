package cn.rdtimes.wolfdsp.sdk.util;


import cn.rdtimes.wolfdsp.sdk.TaskState;

import java.util.Map;

/**
 * 响应工具
 *
 * @author BZ
 */
public final class ResponseUtil {
    public static String getMasterIp(Map<String, Object> params) {
        String ipAndPort = getValue(params, "masterIp", null);
        if (StringUtil.isEmpty(ipAndPort)) return null;
        return (ipAndPort.split(":")[0]);
    }

    public static int getMasterPort(Map<String, Object> params) {
        String ipAndPort = getValue(params, "masterIp", null);
        if (StringUtil.isEmpty(ipAndPort)) return -1;
        return (Integer.parseInt(ipAndPort.split(":")[1]));
    }

    public static String getStandbyIp(Map<String, Object> params) {
        String ipAndPort = getValue(params, "standbyIp", null);
        if (StringUtil.isEmpty(ipAndPort)) return null;
        return (ipAndPort.split(":")[0]);
    }

    public static int getStandbyPort(Map<String, Object> params) {
        String ipAndPort = getValue(params, "standbyIp", null);
        if (StringUtil.isEmpty(ipAndPort)) return -1;
        return (Integer.parseInt(ipAndPort.split(":")[1]));
    }

    public static String getJobId(Map<String, Object> params, String defaultValue) {
        return getValue(params, "jobId", defaultValue);
    }

    public static String getTaskId(Map<String, Object> params, String defaultValue) {
        return getValue(params, "taskId", defaultValue);
    }

    public static long getTimestamp(Map<String, Object> params, long defaultValue) {
        return getValue(params, "timestamp", defaultValue);
    }

    public static String getInputParams(Map<String, Object> params, String defaultValue) {
        return getValue(params, "inputParams", defaultValue);
    }

    private static <T> T getValue(Map<String, Object> params, String key, T defaultValue) {
        Object ret = params.get(key);
        return ret == null ? defaultValue : (T) ret;
    }

    public static void buildCommRsp(Map<String, Object> resp, String jobId, String taskId, TaskState state,
                                    long timestamp, int progress, String exceptionInfo, Map<String, Object> outputParams) {
        resp.put("jobId", jobId);
        resp.put("taskId", taskId);
        resp.put("state", state.getValue());
        resp.put("timestamp", timestamp);
        resp.put("exception", exceptionInfo);
        resp.put("curProgress", progress);
        resp.put("outputParams", outputParams);
    }

    public static void buildCommRsp(Map<String, Object> resp, String jobId, String taskId, TaskState state,
                                    long timestamp, int progress, String exceptionInfo, boolean haveScheduleTask,
                                    Map<String, Object> outputParams) {
        resp.put("jobId", jobId);
        resp.put("taskId", taskId);
        resp.put("state", state.getValue());
        resp.put("timestamp", timestamp);
        resp.put("exception", exceptionInfo);
        resp.put("curProgress", progress);
        resp.put("outputParams", outputParams);
        resp.put("haveScheduleTask", haveScheduleTask);
    }

    private ResponseUtil() {
        // nothing
    }

}
