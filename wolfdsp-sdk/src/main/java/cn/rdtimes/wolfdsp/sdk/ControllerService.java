package cn.rdtimes.wolfdsp.sdk;

import cn.rdtimes.wolfdsp.sdk.util.JsonUtil;
import cn.rdtimes.wolfdsp.sdk.util.ResponseUtil;

import java.util.Map;

/**
 * 用于服务端请求的处理,controller层
 *
 * @author BZ
 */
public final class ControllerService {
    private static ControllerService instance = null;

    public ControllerService() {
        init();
    }

    private synchronized void init() {
        if (instance != null) {
            throw new IllegalStateException("ScheduleService instance is singleton");
        }
        instance = this;
    }

    public static ControllerService getInstance() {
        return instance;
    }

    public ResponseMessage<?> processStart(Map<String, Object> request) {
        try {
            TaskStartMeta meta = createTaskStartMeta(request);
            Map<String, Object> resp = TaskService.getInstance().addScheduleTask(meta);
            return ResponseMessage.success(resp);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseMessage.error(e.getMessage());
        }
    }

    public ResponseMessage<?> processStop(Map<String, Object> request) {
        try {
            String jobId = ResponseUtil.getJobId(request, null);
            String taskId = ResponseUtil.getTaskId(request, null);
            long timestamp = ResponseUtil.getTimestamp(request, System.currentTimeMillis());
            Map<String, Object> resp = TaskService.getInstance().removeScheduleTask(jobId, taskId, timestamp);
            return ResponseMessage.success(resp);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseMessage.error(e.getMessage());
        }
    }

    public ResponseMessage<?> processQueryState(Map<String, Object> request) {
        try {
            String jobId = ResponseUtil.getJobId(request, null);
            String taskId = ResponseUtil.getTaskId(request, null);
            long timestamp = ResponseUtil.getTimestamp(request, System.currentTimeMillis());
            Map<String, Object> resp = TaskService.getInstance().queryScheduleTask(jobId, taskId, timestamp);
            return ResponseMessage.success(resp);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseMessage.error(e.getMessage());
        }
    }

    private TaskStartMeta createTaskStartMeta(Map<String, Object> request) {
        TaskStartMeta meta = new TaskStartMeta();
        meta.setJobId(ResponseUtil.getJobId(request, null));
        meta.setTaskId(ResponseUtil.getTaskId(request, null));
        meta.setMasterIp(ResponseUtil.getMasterIp(request));
        meta.setMasterPort(ResponseUtil.getMasterPort(request));
        meta.setStandbyIp(ResponseUtil.getStandbyIp(request));
        meta.setStandbyPort(ResponseUtil.getStandbyPort(request));
        meta.setTimestamp(ResponseUtil.getTimestamp(request, System.currentTimeMillis()));
        meta.setInputParams(JsonUtil.toMap(ResponseUtil.getInputParams(request, null)));
        return meta;
    }

}
